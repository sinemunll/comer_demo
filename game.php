<?php

$questions = array(" Aklında tuttuğun şey canlı mı?", "Bu şey düşünebilir mi?", "Bu şey miyavlar mı?", "Bu şey bir yazılım mı?");
$words = array("İNSAN, KEDİ,COMER");
$param = 0;
if (isset($_GET['soru'])) {
    $param = htmlspecialchars($_GET["soru"]);
}


switch ($param) {
    case 0:
        $string = "<a href='?soru=1'>Evet</a>&nbsp;<a href='?soru=3'>Hayır</a>";
        break;
    case 1:
        $string = "<a href='?yaz=İNSAN'>Evet</a>&nbsp;<a href='?soru=2'>Hayır</a>";
        break;
    case 2:
        $string = "<a href='?yaz=KEDİ'>Evet</a>&nbsp;<a href='index.php'>Hayır</a>";
        break;
    case 3:
        $string = "<a href='?yaz=COMER'>Evet</a>&nbsp;<a href='index.php'>Hayır</a>";
        break;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tahmin Oyunu</title>
    <link href="main.css" rel="stylesheet"/>
</head>
<body>

<div id="game-scene">
    Şunlardan birini aklında tut:
        <?php
          foreach ($words as $word)
              echo $word ;

        ?>

    <br/>
    <div class="question">
        <?php
        if (!isset($_GET['yaz'])) {
            echo $questions[$param] . '<br/>';
            echo $string;
        }
        ?>
    </div>

    <div class="answer">
        <?php

        if (isset($_GET['yaz'])) {
            echo 'Cevap:' . htmlspecialchars($_GET["yaz"]);
            echo '<a href="game.php" id="game-over"> Yeniden Başla </a>';
        }
        ?>
    </div>


</div>

<script type="text/javascript">


</script>
</body>
</html>
